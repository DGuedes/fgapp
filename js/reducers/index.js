import articlesReducer from './articles-reducer';
import professorsReducer from './professors-reducer';

export {
  articlesReducer,
  professorsReducer
}