import ArticleService from './article-service';
import ProfessorsService from './professors-service';

export {
  ArticleService,
  ProfessorsService
}
